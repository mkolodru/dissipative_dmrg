function parse_index(ind, ind_sz)
    val=ind-1
    ind_out=zeros(Int64,length(ind_sz))
    for j in 1:length(ind_sz)
        ind_out[j]=val % ind_sz[j] + 1
        val=div(val,ind_sz[j])
    end
    return Tuple(ind_out)
end

function hash_index(ind_tup, ind_sz)
    ind=0
    for j in length(ind_sz):-1:1
        ind*=ind_sz[j]
        ind+=(ind_tup[j]-1)
    end
    return ind + 1
end
