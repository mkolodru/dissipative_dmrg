L = 100
J_z = 1
h = 2
chi = 30
init_dir='x' # Direction of spins for initial guess at GS wfn
n_sweeps = 10
alpha_init = 0.1 # Initial value of mixing
alpha_rate = 3 # Rate at which alpha exponentially decays to zero

