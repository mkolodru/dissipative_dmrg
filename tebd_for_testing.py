import numpy as np
from scipy.linalg import expm
import time as time
from mktestparams import *

pauli = np.array([[[0.,1.],[1.,0.]],[[0.,-1.j],[1.j,0.]],
[[1.,0.],[0.,-1.]],[[1.,0.],[0.,1.]]])

emat = np.array([(pauli[3]+pauli[2])/2,(pauli[0]+1.j*pauli[1])/2,
(pauli[0]-1.j*pauli[1])/2,(pauli[3]-pauli[2])/2])

def make_unitary(jx,jy,jz,h1,h2,dt):
        twositeham = jx*np.kron(pauli[0],pauli[0])\
                                +jy*np.kron(pauli[1],pauli[1])\
                                +jz*np.kron(pauli[2],pauli[2])\
                                +.5*h1*np.kron(pauli[2],pauli[3])\
                                +.5*h2*np.kron(pauli[3],pauli[2])
        np.save('twositeham_py.npy',twositeham)

        u = expm(-1.j*dt*twositeham)
        np.save('uop_py.npy',u)

        u = np.tensordot(u,np.conjugate(u),axes=0)
        np.save('u_superop_1_py.npy',u)
        u = np.reshape(u,(2,2,2,2,2,2,2,2))
        np.save('u_superop_2_py.npy',u)
        #         u = np.transpose(u,(0,4,1,5,2,6,3,7))
        np.save('u_superop_3_py.npy',u)
        u = np.reshape(u,(4,4,4,4))
        np.save('u_superop_4_py.npy',u)

        return u


def lindblad_mpo(jx,jy,jz,h1,h2,dt,g1,g2,site):
        twositeham = jx*np.kron(pauli[0],pauli[0])\
                                +jy*np.kron(pauli[1],pauli[1])\
                                +jz*np.kron(pauli[2],pauli[2])\
                                +.5*h1*np.kron(pauli[2],pauli[3])\
                                +.5*h2*np.kron(pauli[3],pauli[2])
        supertwositeham = -1.j*np.kron(twositeham,np.eye(4))\
                                                +1.j*np.kron(np.eye(4),twositeham)

        if site == 0:
                l1 = np.kron(emat[1],pauli[3])
                l2 = np.kron(emat[2],pauli[3])
        else:
                l1 = np.kron(pauli[3],emat[1])
                l2 = np.kron(pauli[3],emat[2])

        lindblad = g1*(2*np.kron(l1,l1)-np.kron(np.dot(l2,l1),np.eye(4))\
                                -np.kron(np.eye(4),np.dot(l2,l1)))\
                                +g2*(2*np.kron(l2,l2)-np.kron(np.dot(l1,l2),np.eye(4))\
                                -np.kron(np.eye(4),np.dot(l1,l2)))
        lindblad = lindblad+supertwositeham
        lindblad = expm(dt*lindblad)

        lindblad = np.reshape(lindblad,(2,2,2,2,2,2,2,2))
        lindblad = np.transpose(lindblad,(0,2,4,6,1,3,5,7))
        lindblad = np.reshape(lindblad,(16,16))
        a,s,v = np.linalg.svd(lindblad,full_matrices=False)

        l = []
        l.append(np.reshape(a,(4,4,1,16)))
        l.append(np.transpose(np.reshape(
                        np.transpose(np.dot(np.diag(s),v)),(4,4,1,16)),(0,1,3,2)))

        return l
def apply_mpo(mpo,w):
        dimw = w.shape
        dimmpo = mpo.shape

        b = np.tensordot(mpo,w,axes=(1,0))
        b = np.transpose(b,(0,1,3,2,4))
        b = np.reshape(b,(4,dimw[1]*dimmpo[2],dimw[2]*dimmpo[3]))
        return b 

def tebd(w,l,u,l1,l2,L,chi,epsilon,bond):
        startsite = 2 if bond == 0 else 1
        endsite = L-2 if bond == 0 else L-1
        #unitary evolution
        for i in range(startsite,endsite,2):
                psi = np.tensordot(w[i],w[i+1],axes=(2,1))
                psi = np.transpose(psi,(0,2,1,3))
                
                phibar = np.tensordot(u,psi,axes=((2,3),(0,1)))
                
                if i!=0:
                        phi = np.tensordot(l[i-1],phibar,axes=(1,2))
                else:
                        phi = np.tensordot(np.array([[1.]]),phibar,axes=(1,2))
                phi = np.reshape(phi,(phi.shape[0]*phi.shape[1],
                                phi.shape[2]*phi.shape[3]))


                a,s,v = np.linalg.svd(phi,full_matrices=False)
                # dim = min(chi,min(phi.shape))

                error = 0
                dim = 0
                cutoff = 0 if chi>s.shape[0] else chi
                for k in range(1,s.shape[0]+1):
                        error += s[-k]**2
                        if np.sqrt(error)>epsilon:
                                dim = s.shape[0]-k+1
                                break

                dim = min(dim,chi)
                
                a = a[:,0:dim]
                s = s[0:dim]
                v = v[0:dim,:]
                        

                l[i] = np.diag(s)/np.sqrt(np.sum(s**2))
        
                w[i+1] = np.reshape(np.transpose(v),(4,phibar.shape[3],s.shape[0]))
                w[i+1] = np.transpose(w[i+1],(0,2,1))
        
                w[i] = np.tensordot(phibar,
                                np.conjugate(np.transpose(w[i+1],(0,2,1))),
                                axes=((1,3),(0,1)))/np.sqrt(np.sum(s**2))
        #boundary dissipation
        if bond == 0:
                w[0] = apply_mpo(l1[0],w[0])
                w[1] = apply_mpo(l1[1],w[1])
                w[-2] = apply_mpo(l2[0],w[-2])
                w[-1] = apply_mpo(l2[1],w[-1])
                        
                for i in range(0,L):
                        dimw = w[i][0].shape
                        wreshape = np.reshape(w[i],(4*dimw[0],dimw[1]))
                        # d = min(wreshape.shape)
                        # d = min(chi,min(wreshape.shape))

                        try:
                                a,s,v = np.linalg.svd(wreshape,full_matrices=False)
                        except:
                                wreshape = wreshape+1e-7*np.eye(4*dimw[0],dimw[1])
                                a,s,v = np.linalg.svd(wreshape,full_matrices=False)

                        error = 0
                        dim = 0
                        cutoff = 0 if chi>s.shape[0] else chi
                        for k in range(1,s.shape[0]+1):
                                error += s[-k]**2
                                if np.sqrt(error)>epsilon:
                                        dim = s.shape[0]-k+1
                                        break

                        d = min(dim,chi)

                        a = a[:,0:d]
                        s = s[0:d]
                        v = v[0:d,:]

                        w[i] = np.reshape(a,(4,w[i].shape[1],s.shape[0]))
                        
                        if i!=L-1:
                                w[i+1] = np.tensordot(np.dot(np.diag(s),v),w[i+1],axes=(1,1))
                                w[i+1] = np.transpose(w[i+1],(1,0,2))
                        else:
                                w[i] = v[0,0]*w[i]
                for i in range(L-1,-1,-1):
                        dimw = w[i][0].shape
                        wreshape = np.transpose(w[i],(0,2,1))
                        wreshape = np.transpose(np.reshape(wreshape,(4*dimw[1],dimw[0])))
                        d = min(wreshape.shape)
                        try:
                                a,s,v = np.linalg.svd(wreshape,full_matrices=False)
                        except:
                                wreshape = wreshape+1e-7*np.eye(dimw[0],4*dimw[1])
                                a,s,v = np.linalg.svd(wreshape,full_matrices=False)
                        a = a[:,0:d]
                        s = s[0:d]
                        v = v[0:d,:]
                        if i!=0:
                                l[i-1] = np.diag(s)/(np.sqrt(np.sum(s**2)))
                        w[i] = np.reshape(np.transpose(v),(4,w[i].shape[2],s.shape[0]))
                        w[i] = np.transpose(w[i],(0,2,1))

                        if i!=0:
                                w[i-1] = np.dot(w[i-1],a*s)/(np.sqrt(np.sum(s**2)))
                        else:
                                w[i] = w[i]*a[0,0]*s[0]
        

                

gl1 = np.sqrt((1.+np.tanh(mul))/(1-np.tanh(mul)))
gl2 = np.sqrt((1.-np.tanh(mul))/(1+np.tanh(mul)))
gr1 = np.sqrt((1.+np.tanh(mur))/(1-np.tanh(mur)))
gr2 = np.sqrt((1.-np.tanh(mur))/(1+np.tanh(mur)))

tau1 = 1/(4-np.cbrt(4.))*dt
tau2 = dt-4*tau1 
M = 20


l = []
for i in range(0,L-1):
        l.append(np.array([[1.]]))

w = []
for i in range(0,L):
        w.append(np.array([[[1./np.sqrt(2.)]],[[0.]],[[0.]],[[1./np.sqrt(2.)]]]))


u11 = make_unitary(jx,jy,jz,h,h,tau1/2)
u21 = make_unitary(jx,jy,jz,h,h,tau1)

u12 = make_unitary(jx,jy,jz,h,h,tau2/2)
u22 = make_unitary(jx,jy,jz,h,h,tau2)

l11 = lindblad_mpo(jx,jy,jz,h,h,tau1,gl1,gl2,0)
l21 = lindblad_mpo(jx,jy,jz,h,h,tau1,gr1,gr2,1)

l12 = lindblad_mpo(jx,jy,jz,h,h,tau2,gl1,gl2,0)
l22 = lindblad_mpo(jx,jy,jz,h,h,tau2,gr1,gr2,1)

identity = np.array([[[1.]],[[0.]],[[0.]],[[1.]]])
z = np.array([[[1.]],[[0.]],[[0.]],[[-1.]]])
osee = np.array([0.])
rn = []
ry2 = []
mry2 = []
res = []

print("Saving stuff before starting")
for j in range(len(w)):
        np.save('w_i_'+str(j)+'_py.npy',w[j])
        if j < len(l):
                np.save('l_i_'+str(j)+'_py.npy',l[j])
np.save('u21_py.npy',u21)
np.save('l11_site0_py.npy',l11[0])
np.save('l11_site1_py.npy',l11[1])
np.save('l21_site0_py.npy',l21[0])
np.save('l21_site1_py.npy',l21[1])

print("Testing even bonds")
tebd(w,l,u21,l11,l21,L,chi,epsilon,0)
for j in range(len(w)):
        np.save('w_even_'+str(j)+'_py.npy',w[j])
        if j < len(l):
                np.save('l_even_'+str(j)+'_py.npy',l[j])

print("Testing odd bonds")
tebd(w,l,u21,l11,l21,L,chi,epsilon,1)
for j in range(len(w)):
        np.save('w_odd_'+str(j)+'_py.npy',w[j])
        if j < len(l):
                np.save('l_odd_'+str(j)+'_py.npy',l[j])

import sys
sys.exit()

print("Starting")
start = time.time()

# print("Before doing anything, we have...")
# print("w=",w)
# print("l=",l)
# print("u11=",u11)
# print("u21=",u21)
# print("l11=",l11)
# print("l21=",l21)
# print("u12=",u12)
# print("u22=",u22)
# print("l12=",l12)
# print("l22=",l22)

for k in range(0,M):
        print(k)

        tebd(w,l,u11,l11,l21,L,chi,epsilon,1)
        tebd(w,l,u21,l11,l21,L,chi,epsilon,0)
        tebd(w,l,u11,l11,l21,L,chi,epsilon,1)

        tebd(w,l,u11,l11,l21,L,chi,epsilon,1)
        tebd(w,l,u21,l11,l21,L,chi,epsilon,0)
        tebd(w,l,u11,l11,l21,L,chi,epsilon,1)

        tebd(w,l,u12,l12,l22,L,chi,epsilon,1)
        tebd(w,l,u22,l12,l22,L,chi,epsilon,0)
        tebd(w,l,u12,l12,l22,L,chi,epsilon,1)

        tebd(w,l,u11,l11,l21,L,chi,epsilon,1)
        tebd(w,l,u21,l11,l21,L,chi,epsilon,0)
        tebd(w,l,u11,l11,l21,L,chi,epsilon,1)

        tebd(w,l,u11,l11,l21,L,chi,epsilon,1)
        tebd(w,l,u21,l11,l21,L,chi,epsilon,0)
        tebd(w,l,u11,l11,l21,L,chi,epsilon,1)

        # print("After step k=",k,", we have...")
        # print("w=",w)
        # print("l=",l)

end = time.time()
print(end-start)

overlap = np.array([[1.]])
for i in range(0,L):
        overlap = np.tensordot(np.conjugate(w[i]),
                np.tensordot(overlap,w[i],axes=(1,1)),axes=((0,1),(1,0)))
print(overlap)
