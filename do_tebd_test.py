import numpy
import subprocess
from mktestparams import L

def call(cmd):
    print(cmd)
    subprocess.call(cmd,shell=True)

call("julia tebd.jl")
call("python tebd_for_testing.py")

# Then compare the results
M_py=numpy.load('twositeham_py.npy')
M_jl=numpy.load('twositeham_jl.npy')
print("For twositeham, max diff is ",max(numpy.abs((M_py-M_jl).flatten())))

M_py=numpy.load('uop_py.npy')
M_jl=numpy.load('uop_jl.npy')
print("For uop, max diff is ",max(numpy.abs((M_py-M_jl).flatten())))

M_py=numpy.load('u_superop_1_py.npy')
M_jl=numpy.load('u_superop_1_jl.npy')
print("For u_superop_1, max diff is ",max(numpy.abs((M_py-M_jl).flatten())))

M_py=numpy.load('u_superop_2_py.npy')
M_jl=numpy.load('u_superop_2_jl.npy')
print("For u_superop_2, max diff is ",max(numpy.abs((M_py-M_jl).flatten())))

M_py=numpy.load('u_superop_3_py.npy')
M_jl=numpy.load('u_superop_3_jl.npy')
print("For u_superop_3, max diff is ",max(numpy.abs((M_py-M_jl).flatten())))

M_py=numpy.load('u_superop_4_py.npy')
M_jl=numpy.load('u_superop_4_jl.npy')
print("For u_superop_4, max diff is ",max(numpy.abs((M_py-M_jl).flatten())))


for j in range(L):
    M_py=numpy.load('w_i_'+str(j)+'_py.npy')
    M_jl=numpy.load('w_i_'+str(j)+'_jl.npy')
    print("For w_i_",j,", max diff is ",max(numpy.abs((M_py-M_jl).flatten())))

    if j < L-1:
        M_py=numpy.load('l_i_'+str(j)+'_py.npy')
        M_jl=numpy.load('l_i_'+str(j)+'_jl.npy')
        print("For l_i_",j,", max diff is ",max(numpy.abs((M_py-M_jl).flatten())))

M_py=numpy.load('u21_py.npy')
M_jl=numpy.load('u21_jl.npy')
print("For u21, max diff is ",max(numpy.abs((M_py-M_jl).flatten())))

M_py=numpy.load('l11_site0_py.npy')
M_jl=numpy.load('l11_site0_jl.npy')
print("For l11, site 0, max diff is ",max(numpy.abs((M_py-M_jl).flatten())))

M_py=numpy.load('l11_site1_py.npy')
M_jl=numpy.load('l11_site1_jl.npy')
print("For l11, site 1, max diff is ",max(numpy.abs((M_py-M_jl).flatten())))

M_py=numpy.load('l21_site0_py.npy')
M_jl=numpy.load('l21_site0_jl.npy')
print("For l21, site 0, max diff is ",max(numpy.abs((M_py-M_jl).flatten())))

M_py=numpy.load('l21_site1_py.npy')
M_jl=numpy.load('l21_site1_jl.npy')
print("For l21, site 1, max diff is ",max(numpy.abs((M_py-M_jl).flatten())))

for j in range(L):
    M_py=numpy.load('w_even_'+str(j)+'_py.npy')
    M_jl=numpy.load('w_even_'+str(j)+'_jl.npy')
    print("For w_even_",j,", max diff is ",max(numpy.abs((M_py-M_jl).flatten())))

    if j < L-1:
        M_py=numpy.load('l_even_'+str(j)+'_py.npy')
        M_jl=numpy.load('l_even_'+str(j)+'_jl.npy')
        print("For l_even_",j,", max diff is ",max(numpy.abs((M_py-M_jl).flatten())))

for j in range(L):
    M_py=numpy.load('w_odd_'+str(j)+'_py.npy')
    M_jl=numpy.load('w_odd_'+str(j)+'_jl.npy')
    print("For w_odd_",j,", max diff is ",max(numpy.abs((M_py-M_jl).flatten())))

    if j < L-1:
        M_py=numpy.load('l_odd_'+str(j)+'_py.npy')
        M_jl=numpy.load('l_odd_'+str(j)+'_jl.npy')
        print("For l_odd_",j,", max diff is ",max(numpy.abs((M_py-M_jl).flatten())))

