using PyPlot
using LinearAlgebra
using NPZ

include("params_tebd_ising_boundary_diss.jl")

t_ed=npzread("t_ed.npy")
C_ed=npzread("C_ed.npy")
sx_ed=npzread("sx_ed.npy")
sz_ed=npzread("sz_ed.npy")

Dt_heis=npzread("Dt.npy")
mps_norm_tebd=npzread("mps_norm.npy")
mpo_norm_tebd=npzread("mpo_norm.npy")
sx_tebd=npzread("sx.npy")
sz_tebd=npzread("sz.npy")
C_tebd=npzread("C.npy")

# for site in 1:L
#     figure(site)
#     plot(t_ed,sx_ed[site,:],label="sx ed")
#     plot(t_ed,sx_tebd[site,:],label="sx tebd")
#     plot(t_ed,sz_ed[site,:],label="sz ed")
#     plot(t_ed,sz_tebd[site,:],label="sz tebd")
#     legend(loc="best")
# end

# figure(L+1)
# plot(t_ed,mps_norm_tebd,label="mps norm")
# plot(t_ed,mpo_norm_tebd,label="mpo norm")
# legend(loc="best")

figure(L+2)
ind=Int64(round(size(C_ed,2)/2))
plot(Dt_heis,real.(C_ed[:,ind]),label="Real(ED)")
plot(Dt_heis,real.(C_tebd[:,ind]),label="Real(TEBD)")
plot(Dt_heis,imag.(C_ed[:,ind]),label="Imag(ED)")
plot(Dt_heis,imag.(C_tebd[:,ind]),label="Imag(TEBD)")
legend(loc="best")

show()
