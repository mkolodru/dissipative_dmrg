# Rewriting code tebd.py into julia

using LinearAlgebra
using TensorOperations
using NPZ

# println(reshape(Matrix([[0,1],[2,3]]),(4,)))
# println(reshape([0 1 2 3],(2,2)))
# println(reshape(reshape([0 1 2 3],(2,2)),(4,)))
# println(reshape([0 1; 2 3],4))
# NOTE TO SELF: JULIA RESHAPES WITH DIFFERENT ORDER THAN PYTHON. SHOULDN'T MATTER IN THE END, BUT INCONVENIENT FOR TESTING.

pauli=[[0 1;1 0],[0 -im;im 0],[1 0;0 -1],[1 0;0 1]]

emat = [(pauli[4]+pauli[3])/2,(pauli[1]+im*pauli[2])/2,
        (pauli[1]-im*pauli[2])/2,(pauli[4]-pauli[3])/2]

"""
make_unitary(jx,jy,jz,h1,h2,dt)

Create unitary U = exp(-i*H*dt) for TEBD of the 2-site Hamiltonian H=jx*x1*x2 + jy*y1*y2 + jz*z1*z2 + 0.5*h1*z1 + 0.5*h2*z2.
Note that, while the unitary will be returned as a 4x4x4x4 tensor, it is not in the correct index permutation for use as an MPO.
"""
function make_unitary(jx,jy,jz,h1,h2,dt)
    twositeham = (jx*kron(pauli[1],pauli[1]) + jy*kron(pauli[2],pauli[2]) + 
                  jz*kron(pauli[3],pauli[3]) + 0.5*h1*kron(pauli[3],pauli[4]) + 
                  0.5*h2*kron(pauli[4],pauli[3])) # Create 2-site Hamiltonian jx*x1*x2 + jy*y1*y2 + jz*z1*z2 + 0.5*h1*z1 + 0.5*h2*z2
    npzwrite("twositeham_jl.npy",twositeham)

    u_op = exp(-im*dt*twositeham) # Unitary for 2 sites as operator acting on ket

    npzwrite("uop_jl.npy",u_op)

    # Convert into superoperator. Not sure why it is complex conjugate and not Hermitian conjugate (***derive this***)
    @tensor u_superop[a,b,c,d]:=u_op[a,b]*conj(u_op[c,d])
    npzwrite("u_superop_1_jl.npy",u_superop)

    u_superop=reshape(u_superop,2,2,2,2,2,2,2,2)
    npzwrite("u_superop_2_jl.npy",u_superop)
    #    u_superop=permutedims(u_superop,(1,5,2,6,3,7,4,8))
    npzwrite("u_superop_3_jl.npy",u_superop)
    u_superop=reshape(u_superop,4,4,4,4)
    npzwrite("u_superop_4_jl.npy",u_superop)
    
    return u_superop
    #    return reshape(permutedims(reshape(u_superop,2,2,2,2,2,2,2,2),(1,5,2,6,3,7,4,8)),4,4,4,4) # Then reshape it into TEBD form (***derive this***)
end

"""
lindblad_mpo(jx,jy,jz,h1,h2,dt,g1,g2,site)

Create MPO form of 2-site Lindblad operator with H=jx*x1*x2 + jy*y1*y2 + jz*z1*z2 + 0.5*h1*z1 + 0.5*h2*z2 and
certain form of Lindblad part (***figure this out***)
"""
function lindblad_mpo(jx,jy,jz,h1,h2,dt,g1,g2,site)
    twositeham = (jx*kron(pauli[1],pauli[1]) + jy*kron(pauli[2],pauli[2]) + jz*kron(pauli[3],pauli[3]) + 
                  0.5*h1*kron(pauli[3],pauli[4]) + 0.5*h2*kron(pauli[4],pauli[3]))

    eye=Matrix(I,4,4)
    supertwositeham = -im*kron(twositeham,eye) + im*kron(eye,twositeham)
    
    if site == 1
	l1 = kron(emat[2],pauli[4])
	l2 = kron(emat[3],pauli[4])
    else
	l1 = kron(pauli[4],emat[2])
	l2 = kron(pauli[4],emat[3])
    end
    
    lindblad = g1*(2*kron(l1,l1)-kron(l2*l1,eye)-kron(eye,l2*l1)) + g2*(2*kron(l2,l2)-kron(l1*l2,eye)-kron(eye,l1*l2))
    lindblad = lindblad+supertwositeham
    lindblad = reshape(permutedims(reshape(exp(dt*lindblad),2,2,2,2,2,2,2,2),(1,3,5,7,2,4,6,8)),16,16)
    F = svd!(lindblad) # lindblad matrix no longer means anything
    a=F.U
    s=F.S
    v=F.Vt

    # ***should I check that s doesn't have trailing zeros that can be dropped?***
    return [reshape(a,4,4,1,16),permutedims(reshape(transpose(Diagonal(s)*v),4,4,1,16),(1,2,4,3))]
end
    
"""
apply_mpo(mpo,w)

Do the product mpo*w. 
mpo is 4-dimensional tensor, w is a 3-dimensional tensor corresponding to on-site part of the MPS.
"""
@tensor function apply_mpo(mpo,w)
    wf[s,b1,a1,b2,a2]:=mpo[s,s_prime,b1,b2]*w[s_prime,a1,a2]
    #    b = np.tensordot(mpo,w,axes=(1,0))
    return reshape(wf,size(wf,1),size(wf,2)*size(wf,3),size(wf,4)*size(wf,5)) # Reshape from [s,b1,a1,b2,a2] to [s,(b1,a1),(b2,a2)]
end

"""
svd_and_trim!(m,epsilon,chi)

Do SVD m = a * Diagonal(s) * v', cut to min of error epsilon on sqrt(sum(s^2)) or max bond dim chi.
To conserve memory, use svd!, meaning that m is trash after running it.
"""
function svd_and_trim!(m,epsilon,chi)
    F=svd!(m) # m is now garbage
    a=F.U
    s=F.S
    vdag=F.Vt # match convention from numpy, which return V'

    error = 0
    dim = 1
    for k in 0:(length(s)-1)
	error += s[end-k]^2
	if sqrt(error) > epsilon
	    dim = length(s)-k
	    break
        end
    end
    
    dim = min(dim,chi)
    
    return (a[:,1:dim], s[1:dim], vdag[1:dim,:])
end

"""
tebd(w,l,u,l1,l2,L,chi,epsilon,bond)

Do TEBD on density matrix stored as site matrices w, bond matrices l, unitary time evolution u, Lindblad MPOs l1 (left edge) and l2 (right edge),
max bond dimension chi, max error epsilon. bond=0 or 1 specifies even or odd bonds.
"""
function tebd(w,l,u,l1,l2,L,chi,epsilon,bond)
    startsite = (bond == 0) ? 3 : 2
    endsite = L-2 #(bond == 0) ? L-1 : L
    one_tensor=ones(1,1) # For use at the edge, 2-dimensional tensor lambda_left = left_right = 1.
    a=nothing
    s=nothing
    vdag=nothing
    
    # unitary evolution on all even/odd bonds except the first and last one
    for i in startsite:2:endsite
        @tensor psibar[s1,s2,a0,a2]:=w[i][s1,a0,a1]*w[i+1][s2,a1,a2] # Apparently w = B matrix = Gamma * Lambda
        #        psi = permutedims(psi,(1,3,2,4)) # Can do this in @tensor, try later

	@tensor phibar[s1,s2,a0,a2] := u[s1,s2,s1_prime,s2_prime]*psibar[s1_prime,s2_prime,a0,a2]
	
	if i!=1
	    @tensor phi[a0,s1,a2,s2] := l[i-1][a0,a1]*phibar[s1,s2,a1,a2]
	else
	    @tensor phi[a0,s1,a2,s2] := onetensor[a0,a1]*phibar[s1,s2,a1,a2]
        end
        phi=reshape(phi,size(phi,1)*size(phi,2),size(phi,3)*size(phi,4)) # Regroup as phi[(a0,s1),(a2,s2)]

	(a,s,vdag) = svd_and_trim!(phi,epsilon,chi) # phi is now garbage

        norm = sqrt(sum(s.^2))
	l[i] = Diagonal(s)./norm
	
        w[i+1] = permutedims(reshape(vdag,length(s),size(phibar,4),size(phibar,1)),(3,1,2)) # reshape from [a1,(a2,s2)] to [a1,a2,s2], then permute to [s2,a1,a2]
	
	@tensor w[i][s1,a0,a1] := phibar[s1,s2,a0,a2]*conj(w[i+1][s2,a1,a2])/norm
    end # for i in startsite:2:endsite
        
    #boundary dissipation
    if bond == 0
	w[1] = apply_mpo(l1[1],w[1])
	w[2] = apply_mpo(l1[2],w[2])
	w[end-1] = apply_mpo(l2[1],w[end-1])
	w[end] = apply_mpo(l2[2],w[end])
	
	for i in 1:L
	    dimw = size(w[i])
	    wreshape = reshape(w[i],dimw[1]*dimw[2],dimw[3]) # reshape from [s1,a1,a2] to [(s1,a1),a2]

            #			try: ***double check why safety was added***
            #			except:
            #				wreshape = wreshape+1e-7*np.eye(4*dimw[0],dimw[1])
            #				a,s,v = np.linalg.svd(wreshape,full_matrices=False)
            try
		(a,s,vdag) = svd_and_trim!(wreshape,epsilon,chi) # wreshape is now garbage
            catch e
		wreshape = reshape(w[i],dimw[1]*dimw[2],dimw[3]) + 1e-7*Matrix(I,dimw[1]*dimw[2],dimw[3]) # Avoid zero singular values?
		(a,s,vdag) = svd_and_trim!(wreshape,epsilon,chi)
            end

            w[i]=reshape(a,size(w[i],1),size(w[i],2),length(s)) # reshape from [(s1,a1),a2] to [s1,a1,a2], where a2 is truncated
            
	    if i!=L
		@tensor w[i+1][s2,a1,a2] := Diagonal(s)[a1,atemp]*vdag[atemp,atemp2]*w[i+1][s2,atemp2,a2]
	    else
		w[i] = vdag[1,1]*w[i]
            end
	end # for i in range(0,L):

	for i in L:-1:1
	    dimw = size(w[i])
	    wreshape = transpose(reshape(permutedims(w[i],(1,3,2)),dimw[1]*dimw[3],dimw[2]))
	    d = minimum(size(wreshape))

	    # try:
	    # 	a,s,v = np.linalg.svd(wreshape,full_matrices=False)
	    # except:
	    # 	wreshape = wreshape+1e-7*np.eye(dimw[0],4*dimw[1])
	    # 	a,s,v = np.linalg.svd(wreshape,full_matrices=False)

            F = nothing # initialize F so I can use it later within this scope. could initialize as an empty SVD class
            try
		F = svd!(wreshape)
            catch e
		wreshape = transpose(reshape(permutedims(w[i],(1,3,2)),dimw[1]*dimw[3],dimw[2])) + 1e-7*Matrix(I,dimw[2],dimw[1]*dimw[3])
		F = svd!(wreshape)
                # println("Having trouble doing svd")
                # println("size(wreshape)=",size(wreshape))
                # println("wreshape=")
                # println(wreshape)
                # println("error=")
                # println(e)
                # exit()
            end

            a=F.U
            s=F.S
            vdag=F.Vt # match convention from numpy, which return V'

            # println("Before cut")
            # println("size(vdag)=",size(vdag))
            # println("size(a)=",size(a))
            # println("size(s)=",size(s))
            # println("d=",d)

	    a = a[:,1:d]
	    s = s[1:d]
	    vdag = vdag[1:d,:]
            norm=sqrt(sum(s.^2))
	    if i!=1
		l[i-1] = Diagonal(s)/norm
            end
            # println("After cut")
            # println("size(wreshape)=",size(wreshape))
            # println("size(vdag)=",size(vdag))
            # println("size(a)=",size(a))
            # println("size(s)=",size(s))
            # println("size(w[i])=",size(w[i]))
            # println("i=",i)
	    w[i] = permutedims(reshape(transpose(vdag),size(w[i],1),size(w[i],3),size(s,1)),(1,3,2))
            
	    if i!=1
                rmul=a*l[i-1]
                @tensor w[i-1][s0,a0,a1] = w[i-1][s0,a0,amid]*rmul[amid,a1]
		# w[i-1] = w[i-1]*(a*s)/norm
	    else
		w[i] = w[i]*a[1,1]*s[1]
            end
        end # for i in range(L-1,-1,-1)
    end # if bond == 0
end # function tebd
	
function main()
    
    include("mktestparams.py")

    gl1 = sqrt((1+tanh(mul))/(1-tanh(mul)))
    gl2 = sqrt((1-tanh(mul))/(1+tanh(mul)))
    gr1 = sqrt((1+tanh(mur))/(1-tanh(mur)))
    gr2 = sqrt((1-tanh(mur))/(1+tanh(mur)))
    
    tau1 = 1/(4-4^(1/3))*dt
    tau2 = dt-4*tau1 
    M = 1 # Number of time stepsilon dt
    
    l = []
    w = []
    l0=ones(1,1)
    w0=zeros(4,1,1)
    w0[1,1,1]=1/sqrt(2)
    w0[4,1,1]=1/sqrt(2)
    for i in 1:L
	push!(l,copy(l0))
	push!(w,copy(w0)) # Initial state appears to be x=1 on each site
    end
    
    u11 = make_unitary(jx,jy,jz,h,h,tau1/2)
    u21 = make_unitary(jx,jy,jz,h,h,tau1)
    
    u12 = make_unitary(jx,jy,jz,h,h,tau2/2)
    u22 = make_unitary(jx,jy,jz,h,h,tau2)
    
    l11 = lindblad_mpo(jx,jy,jz,h,h,tau1,gl1,gl2,0)
    l21 = lindblad_mpo(jx,jy,jz,h,h,tau1,gr1,gr2,1)
    
    l12 = lindblad_mpo(jx,jy,jz,h,h,tau2,gl1,gl2,0)
    l22 = lindblad_mpo(jx,jy,jz,h,h,tau2,gr1,gr2,1)
    
    identity = w0*sqrt(2)
    z = copy(identity)
    z[4,1,1]=-1
    osee = [0]
    rn = []
    ry2 = []
    mry2 = []
    res = []

    println("Saving stuff before starting")
    for j in 1:length(w)
        npzwrite("w_i_"*string(j-1)*"_jl.npy",w[j])
        if j <= length(l)
            npzwrite("l_i_"*string(j-1)*"_jl.npy",Matrix(l[j]))
        end
    end
    npzwrite("u21_jl.npy",u21)
    npzwrite("l11_site0_jl.npy",l11[1])
    npzwrite("l11_site1_jl.npy",l11[2])
    npzwrite("l21_site0_jl.npy",l21[1])
    npzwrite("l21_site1_jl.npy",l21[2])
    
    println("Testing even bonds")
    tebd(w,l,u21,l11,l21,L,chi,epsilon,0)
    for j in 1:length(w)
        npzwrite("w_even_"*string(j-1)*"_jl.npy",w[j])
        if j <= length(l)
            npzwrite("l_even_"*string(j-1)*"_jl.npy",Matrix(l[j]))
        end
    end
    
    println("Testing odd bonds")
    tebd(w,l,u21,l11,l21,L,chi,epsilon,1)
    for j in 1:length(w)
        npzwrite("w_odd_"*string(j-1)*"_jl.npy",w[j])
        if j <= length(l)
            npzwrite("l_odd_"*string(j-1)*"_jl.npy",Matrix(l[j]))
        end
    end
    
    exit()



    println("Starting")
    start = time()

    # println("Before doing anything, we have...")
    # println("w=",w)
    # println("l=",l)

    for k in 1:M
	println("At time step ",k)

	tebd(w,l,u11,l11,l21,L,chi,epsilon,1)
	tebd(w,l,u21,l11,l21,L,chi,epsilon,0)
	tebd(w,l,u11,l11,l21,L,chi,epsilon,1)

	tebd(w,l,u11,l11,l21,L,chi,epsilon,1)
	tebd(w,l,u21,l11,l21,L,chi,epsilon,0)
	tebd(w,l,u11,l11,l21,L,chi,epsilon,1)

	tebd(w,l,u12,l12,l22,L,chi,epsilon,1)
	tebd(w,l,u22,l12,l22,L,chi,epsilon,0)
	tebd(w,l,u12,l12,l22,L,chi,epsilon,1)

	tebd(w,l,u11,l11,l21,L,chi,epsilon,1)
	tebd(w,l,u21,l11,l21,L,chi,epsilon,0)
	tebd(w,l,u11,l11,l21,L,chi,epsilon,1)

	tebd(w,l,u11,l11,l21,L,chi,epsilon,1)
	tebd(w,l,u21,l11,l21,L,chi,epsilon,0)
	tebd(w,l,u11,l11,l21,L,chi,epsilon,1)

        # println("After step k=",k,", we have...")
        # println("w=",w)
        # println("l=",l)
    end

    stop = time()
    println("Total time = ",stop-start)
    
    overlap = ones(1,1)
    for i in 1:L
        @tensor overlap2[a1,a2] := conj(w[i][s1,a3,a1])*overlap[a3,a4]*w[i][s1,a4,a2]
        overlap=copy(overlap2)
    end
    println("overlap=",overlap)
end

main()
