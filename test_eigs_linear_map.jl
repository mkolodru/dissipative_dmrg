using LinearMaps
import Arpack #, IterativeSolvers, KrylovKit, TSVD
using SparseArrays
using Random

Random.seed!(0)
basis_sz=5
H=spzeros(ComplexF64,basis_sz,basis_sz)
for j in 1:10
    H[rand(1:basis_sz),rand(1:basis_sz)]=randn()+im*randn()
end
H=H+H'

function apply_H!(y::AbstractVector, x::AbstractVector, mult)
    # Need to make sure each operation mutates y rather than redefining it
    fill!(y,0)
    y.+=mult*H*x
    #    return y
end

# Example 1, 1-dimensional Laplacian with periodic boundary conditions
function leftdiff!(y::AbstractVector, x::AbstractVector) # left difference assuming periodic boundary conditions
    N = length(x)
    length(y) == N || throw(DimensionMismatch())
    @inbounds for i=1:N
        y[i] = x[i] - x[mod1(i-1, N)]
    end
    return y
end

function mrightdiff!(y::AbstractVector, x::AbstractVector) # minus right difference
    N = length(x)
    length(y) == N || throw(DimensionMismatch())
    @inbounds for i=1:N
        y[i] = x[i] - x[mod1(i+1, N)]
    end
    return y
end

println("Doing their linear map")

D = LinearMap(leftdiff!, mrightdiff!, 100; ismutating=true) # by default has eltype(D) = Float64

(E,v)=Arpack.eigs(D'D; nev=3, which=:SR) # note that D'D is recognized as symmetric => real eigenfact
println("E=",E)

println("Doing my linear map")

# x=randn(basis_sz)+im*randn(basis_sz)
# y=randn(basis_sz)+im*randn(basis_sz)
# y2=H*x
# apply_H!(y,x)
# println("y=",y)
# println("y2=",y2)

mult=2
M = LinearMap{ComplexF64}((y,x)->apply_H!(y,x,mult), basis_sz; ismutating=true, ishermitian=true)

(E,v)=Arpack.eigs(M; nev=3, which=:SR)
println("E=",E)
(E_usual,v_usual)=Arpack.eigs(H; nev=3, which=:SR)
println("E_usual=",E_usual)

