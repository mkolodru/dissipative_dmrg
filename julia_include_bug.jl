function f1()
    
    println("In f1()")

    include("params.jl")

    println("In f1(), jz=",jz)

end

function f2()
    
    println("In f2()")

    include("params.jl")

    println("In f2(), jz=",jz)

    jz = 0

    println("In f2(), after reset, jz=",jz)

end

f1()
f2()

