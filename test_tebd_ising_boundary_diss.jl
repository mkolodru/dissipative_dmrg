using LinearAlgebra
using NPZ
using Arpack

# Run TEBD
include("tebd_ising_boundary_diss.jl")

# Run ED
function get_sz(u,site)
    return ((u-1) & (1<<(site-1))==0) ? 1 : -1 # u-1 gives binary in range 0 to 2^L-1
end

function flip_site(u,site)
    return xor(u-1,1<<(site-1))+1
end

include("params_tebd_ising_boundary_diss.jl")
basis_sz=2^L
H=zeros(basis_sz,basis_sz)
x_site=zeros(L,basis_sz,basis_sz)
z_site=zeros(L,basis_sz,basis_sz)

for u in 1:basis_sz
    for site in 1:L
        if site != L
            H[u,u]-=J_z*get_sz(u,site)*get_sz(u,site+1)
        end
        u2=flip_site(u,site)
        if u2 < 1 || u2 > basis_sz
            println("issue finding u2 for u=",u," or ",bitstring(u)[end-L+1:end],", site=",site,", u2=",u2," or ",bitstring(u2)[end-L+1:end])
            exit()
        end
        H[u,u2]=-h
        x_site[site,u,u2]=1
        z_site[site,u,u]=get_sz(u,site)
    end
end

L_z=sqrt(gamma)*reshape(z_site[1,:,:],basis_sz,basis_sz) # Lindblad operator on boundary

# Start out in ground state
# (E,psi)=eigs(H,nev=1,which=:SR)

# Start pointing up
psi=zeros(basis_sz)
psi[1]=1

# Start pointing right
psi=ones(basis_sz)
psi=psi./norm(psi)

# Time evolve with Lindblad

Dt_heis=npzread("Dt.npy")

sx_ed=zeros(L,n_steps_time_ev+1)
sz_ed=zeros(L,n_steps_time_ev+1)
C_ed=zeros(ComplexF64,length(Dt_heis),n_steps_time_ev+1)

t_ed=collect(0:dt:(n_steps_time_ev+0.5)*dt)

rho=zeros(ComplexF64,basis_sz,basis_sz)
rho[:,:]=psi*psi'

using DifferentialEquations

function drho_dt(rho,p,t)
    return -im*H*rho+im*rho*H+L_z*rho*L_z-0.5*(L_z*L_z*rho + rho*L_z*L_z)
end

tspan = (0.0,t_ed[end])
prob = ODEProblem(drho_dt,rho,tspan)
println("Solving ODE")
sol = solve(prob, Tsit5(), reltol=1e-8, abstol=1e-8)

# for site in 1:L
#     sx_ed[site,1]=real(tr(rho*reshape(x_site[site,:,:],(basis_sz,basis_sz))))
#     sz_ed[site,1]=real(tr(rho*reshape(z_site[site,:,:],(basis_sz,basis_sz))))
# end

for j in 1:length(t_ed)
    println("Evaluating at step ",j," out of ",n_steps_time_ev)
    rho=sol(t_ed[j])
    for site in 1:L
        sx_ed[site,j]=real(tr(rho*reshape(x_site[site,:,:],(basis_sz,basis_sz))))
        sz_ed[site,j]=real(tr(rho*reshape(z_site[site,:,:],(basis_sz,basis_sz))))
    end

    sz_times_rho=reshape(z_site[1,:,:],(basis_sz,basis_sz))*rho
    tspan_heis = (0.0,Dt_heis[end])
    prob_heis = ODEProblem(drho_dt,sz_times_rho,tspan_heis)
    println("Solving Heisenberg ODE")
    sol_heis = solve(prob_heis, Tsit5(), reltol=1e-8, abstol=1e-8)
    for k in 1:length(Dt_heis)
        sz_times_rho=sol_heis(Dt_heis[k])
        C_ed[k,j]=tr(sz_times_rho*reshape(z_site[1,:,:],(basis_sz,basis_sz)))
    end
end

npzwrite("t_ed.npy",t_ed)
npzwrite("sx_ed.npy",sx_ed)
npzwrite("sz_ed.npy",sz_ed)
npzwrite("C_ed.npy",C_ed)

# Import TEBD data

mps_norm_tebd=npzread("mps_norm.npy")
mpo_norm_tebd=npzread("mpo_norm.npy")
sx_tebd=npzread("sx.npy")
sz_tebd=npzread("sz.npy")
C_tebd=npzread("C.npy")

#site=1#Int64(round(L/2))

println("Max diff of sx, time ev = ",maximum(abs.(sx_ed-sx_tebd)))
println("Max diff of sz, time ev = ",maximum(abs.(sz_ed-sz_tebd)))
println("Max diff of C = ",maximum(abs.(C_ed-C_tebd)))

try
    using PyPlot
    for site in 1:L
        figure(site)
        plot(t_ed,sx_ed[site,:],label="sx ed")
        plot(t_ed,sx_tebd[site,:],label="sx tebd")
        plot(t_ed,sz_ed[site,:],label="sz ed")
        plot(t_ed,sz_tebd[site,:],label="sz tebd")
        legend(loc="best")
    end
    
    figure(L+1)
    plot(t_ed,mps_norm_tebd,label="mps norm")
    plot(t_ed,mpo_norm_tebd,label="mpo norm")
    legend(loc="best")
    
    figure(L+2)
    ind=Int64(round(size(C_ed,2)/2))
    plot(Dt_heis,real.(C_ed[:,ind]),label="Real(ED)")
    plot(Dt_heis,real.(C_tebd[:,ind]),label="Real(TEBD)")
    plot(Dt_heis,imag.(C_ed[:,ind]),label="Imag(ED)")
    plot(Dt_heis,imag.(C_tebd[:,ind]),label="Imag(TEBD)")
    legend(loc="best")
    
    show()
catch e
    println("Unable to do plotting...")
end

