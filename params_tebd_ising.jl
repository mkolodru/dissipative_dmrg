L = 6
jz = 1
h = 1
chi = 100
epsilon = 1e-7
dt = 0.05
dt_heis_out=0.05 # Time between outputing data for Heisenberg time evolution (code will force to be a multiple of dt)
t_max = 2
t_max_heis = 2 # For heisenberg evolution of sz operator to measure 2-time correlation function
tau_max = 2 # For imaginary time evolution to ground state
gamma = 1
n_steps_time_ev = Int64(round(t_max/dt))
n_steps_imag_time_ev = Int64(round(tau_max/dt))
init_dir='x'

