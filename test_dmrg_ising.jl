using LinearAlgebra
using NPZ
using Arpack

# Run DMRG
include("dmrg_ising.jl")

# Run ED
function get_sz(u,site)
    return ((u-1) & (1<<(site-1))==0) ? 1 : -1 # u-1 gives binary in range 0 to 2^L-1
end

function flip_site(u,site)
    return xor(u-1,1<<(site-1))+1
end

include("params_dmrg_ising.jl")

# basis_sz=2^L
# H=spzeros(basis_sz,basis_sz)

# for u in 1:basis_sz
#     for site in 1:L
#         if site != L
#             H[u,u]=H[u,u]-J_z*get_sz(u,site)*get_sz(u,site+1)
#         end
#         u2=flip_site(u,site)
#         H[u,u2]=-h
#     end
# end

# (E_gs,gs)=eigs(Hermitian(H),nev=1,which=:SR)

# Get ground state energy using free ptcl Hamiltonian
H_free=zeros(2*L,2*L)
for j in 1:(2*L-1)
    if j % 2 == 1
        H_free[j,j+1]=h
    else
        H_free[j,j+1]=J_z
    end
end
E_free=eigvals(H_free+H_free')
E_gs=-sum(abs.(E_free))/2

# println("E_gs=",E_gs)
println("E_gs_free=",E_gs_free)

E=npzread("E.npy")
sweep=collect(1:length(E))

try
    using PyPlot
    plot(sweep,E,"r")
    plot(sweep,0*sweep.+E_gs,"k--")
    show()
catch e
    println("Unable to do plotting...")
end
