using DifferentialEquations
using Random
using LinearAlgebra

Random.seed!(0)

H=randn(10,10)+im*randn(10,10)
H=H+H'
t=1.5

psi=randn(10)+im*randn(10)
psi=reshape(psi/norm(psi),10,1)

rho=psi*psi'

U=exp(-im*H*t)

rho_easy=U*rho*U'

function drho_dt(rho,p,t)
    return -im*H*rho+im*rho*H
end

tspan = (0.0,t)
prob = ODEProblem(drho_dt,rho,tspan)
sol = solve(prob, Tsit5(), reltol=1e-8, abstol=1e-8)

rho_ode=sol(t)

println("Max diff is ",maximum(abs.(rho_ode-rho_easy)))
